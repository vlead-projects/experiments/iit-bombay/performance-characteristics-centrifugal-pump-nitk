## Pedagogy (Round 1)
<p align="center">
<br>
<br>
<b> Experiment: Newton's Ring Experiment  <a name="top"></a> <br>
</p>

<b>Discipline | <b>Engineering
:--|:--|
<b> Lab | <b> Engineering Physics
<b> Experiment|     <b> 1. Determining wavelength of light and refractive index using Newton's ring experiment.


<h4> [1. Focus Area](#LO)
<h4> [2. Learning Objectives ](#LO)
<h4> [3. Instructional Strategy](#IS)
<h4> [4. Task & Assessment Questions](#AQ)
<h4> [5. Simulator Interactions](#SI)
<hr>

<a name="LO"></a>
#### 1. Focus Area : Experimentation and Data Analysis
The students know the concept of interference, value of wavelength of light source used and relation for finding the wavelength of light and refractive index of the liquid using Newton’s ring experiment. They do the experimentation based on the formula, trial and error and learn from it, finally analyzing results with standard value.
#### 2. Learning Objectives and Cognitive Level


Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>Identify instruments used in experiment Plano-convex lens, Sodium lamp, Travelling microscope etc<br> | Recall | Identify
2.| User will be able to: <br>Define path difference, wavelength of light, and least count.<br> | Recall | Define
3.| User will be able to: <br>Describe how light rays interfere to produce rings.  | Understand | Describe
4.| User will be able to: <br>Explain the relation between wavelength of light, dark rings, bright rings and refractive index of medium | Understand | Explain
5.| User will be able to: <br>Calculate diameters of rings | Apply | Calculate
6.| User will be able to: <br>Calculate the wavelength of light source and refractive index by putting readings recorded on Travelling microscope  | Apply | Calculate
7.| User will be able to: <br>Plot a graph of ring number (x-axis) versus D<sub>n</sub><sup>2</sup> (y-axis) | Apply | Plot 
8.| User will be able to: <br>Infer on percent error found in the value of wavelength of light  | Analyze | Infer
9.| User will be able to: <br>Examine the change in rings pattern with change in medium | Analyze | Examine


<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="IS"></a>
#### 3. Instructional Strategy
###### Name of Instructional Strategy  :    <u> Expository
###### Assessment Method: Formative Assessment

<u> <b>Description:</b></u> <u> Instructional Strategy will be implemented in the simulator as follows: </u>
<br>
 Students will identify the various components as per the arrangement to construct the setup. With this, they will get concept and purpose of each component. They will set the travelling microscope to get the properly resolved image. The students put the cross wire on specific rings. Based on readings, they will get the diameters of the rings. These values will be put in the formula from which wavelength of light and refractive index of the liquid shall be calculated. After performing the experiment, student shall give answer to the questions asked.

<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="AQ"></a>
#### 4. Task & Assessment Questions:

Read the theory and comprehend the concepts related to the experiment. [LO1, LO2, LO3]
<br>

Sr. No |	Learning Objective	| Task to be performed by <br> the student  in the simulator | Assessment Questions as per LO & Task
:--|:--|:--|:--
1.| Students will be able to identify instruments used in experiment such as Plano-convex lens, Sodium lamp, Travelling microscope | On the simulator screen, student selects proper lens and other objects in the set up to drag and drop at proper place as per the experimental setup. | Q Drag and drop correct apparatus and labels to various components of experimental setup. <br>Q. Which one of the following lens is used for interference to occur in Newton’s ring experiment?<br>A. Convex Lens <br>B. Plano Convex Lens <br>C. BiConcave Lens <br>D. Plano Concave Lens
2.| Students will be able to define path difference, wavelength of light and least count | On simulator screen, student observe the experimental set up and attempt the question | What is the condition for constructive interference pattern?<br>A. integral multiple of wavelength<br>B. half multiples of wavelength
3.| Students will be able to describe how light rays interfere to produce rings.  | On simulator screen, student set the medium of interference as air from select medium option seen and answer the true or false question  | Q  The necessary condition for interference is<br> A. The sources of the waves must be coherent, constant phase difference. <br> B. The waves should be monochromatic, single wavelength <br> C. A and B both <br>D. none of A and B
4.| Students will be able to note the readings on the scale by using Travelling microscope. | Set the position of the plane glass plate at 45 <sup>o</sup>. Switch on the monochromatic source. Set travelling microscope at proper resolution position, so that the rings are visible. Control the position of the movement screw of the travelling microscope so as to set it at tangential position of the ring and the ring diameter should be noted down. | Q. What is the role of glass plate set at 45<sup>o</sup>?<br>A. To pass the light<br>B. To deflect the light downward direction <br>C. To increase the illumination <br>D. To diffract the light <br>Q. What is the least count of the travelling microscope?<br>Answer: 0.01 mm
5.| Students will be able to calculate diameter of Newton's rings | On the simulator screen, student change the position of the cross wire from one side of the image to other side of image by pressing the “Traverse” button. Student set the cross wire is at tangential position. Add the ring diameter in the observation table. | Q.Fill in the blank with proper option Newton’s rings are ______ and as moved away from the centre, the rings appear ________ and _________. <br>Answer: Concentric, thinner, closer
6.| Students will be able to calculate wavelength of light and Refractive index using observed readings| Student submit the value of answer in the field provided on the simulator screen  | Q. Identify the formula of wavelength of light and refractive index in Newton’s ring experiment.<br>Q. In a Newton’s rings experiment the diameter of the 15th ring was found to be 0.59 cm and that of the 5th ring is 0.336 cm. If the radius of curvature of the lens is 100 cm, find the wave length of the light.
7.| Students will be able to plot a graph of ring number versus square of diameter | On simulator screen, a graph will be plotted from the collected readings | The nature of graph is <br>A. Straight line<br>B. Inclined line with negative slope<br>C. Inclined line with positive slope<br>D. None of the options
8.| Students will be able to infer on percent error found in the value of wavelength of light | On the simulator, student enters percent error value of the result in the field provided. | Q. Whether the theoretical value of wavelength of light is matching with calculated value?
9.| Students will be able to examine the change in rings pattern with change in medium | On the simulator, student change the medium from air to any other medium and examine the change in ring pattern  | Q. When a medium other than air is present between curved and plane surface of the lens, the Newton’s rings shrink. True or false <br>Q.What is the reason for change in ring pattern when a medium other than air is present?<br>A. The wavelength in water in denser medium is less than that in air<br>B. The wavelength in water in denser medium is more than that in air<br>C. Radius of Curvature of lens<br>D. None of the options
</div>
<br>

<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="SI"></a>

#### 4. Simulator Interactions:
<br>

Sr.No | What students will do? | What Simulator will do ? | Purpose of the task
:--|:--|:--|:--:
1.| Student will click on Simulation tab | Simulator opens the experiment screen | To open screen to perform experiment 
2.| Student will drag and drop various apparatus parts at appropriate positions on the blank canvas.  | Simulator will display a blank canvas appears on the screen and all apparatus parts separately and provide option to drag and drop the parts at appropriate position. <br>If student located it wrongly appropriate message will be displayed. | To recall the set-up
3.| Student will confirm the labeling of the experimental set-up appearing on the simulator  | Simulator will display labeled image with rays of light complete ray movement   | To provide basic environment to start the experiment.
4.| Student calculate least count and enters the value of least count in the field provided  | Simulator will display main scale and verniers scale with enlarge view Simulator accepts the input of least count value and if the student is correct display massage “correct least count” or else gives the hint to get correct value  | To let the student understand concept of least count.
5.| Student will move the screw of the travelling microscope and set the cross wire at tangential position. Note readings for various rings. | Simulator will allow the student to change the position of the cross wire from one side of the image to other side of image when the student press the “Traverse” button. Put the cross wire is at tangential position.  | To arrange the experimental condition
6.| Student will click on the “Add to Readings” button to store the ring diameter value in the observation table  | Simulator will display the “Add to Readings” Button and store the ring diameter value in the observation table. | To record and store readings
7.| Student will click on the “Display Table”  button to see the observation table. <br>Student will click on the “Reset”  button | Simulator will display the observation table and plot the graph with given set of readings. Simulator will reset the conditions to original  | To observe the recorded readings and plot the graph.<br>To reset the situation with the initial conditions.  
8.| Student will calculate the value and enter the answer in input text box and click the submit button, with the help of formula for wavelength and refractive index | Simulator will display input text box and button “submit”, where student will enter calculated wavelength and refractive index. If answer is correct: Display the success message and record the result. If answer is wrong : provide clue and Display input text box and button “submit” again | To make the student calculate the wavelength of light To check for the calculated value whether it is correct or not.
9.| Student  changes the medium from the list of mediums and observes the rings pattern  | Simulator display the corresponding image for the selected medium and display the question <br>Q.| The reason of change in image is,  <br>a. instrument resolution power <br>b. change in the medium | To accept the input from student and provide feedback to student that the images shrink when there is a medium other than air present.
